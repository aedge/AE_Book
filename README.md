## 页面展示效果
打开项目，查看展示页面方法
```
	页面存放地址 /src/App.vue
	npm run serve
```
### 组件说明
#### cover

作用：定义一个类似word文档的首页

##### 调用方法
> 将组件迁移至项目，在页面引入cover所在地址，并在components中直接引入 即可
> 该组件依赖page布局

##### 属性

| 属性名                  | 作用                                                     | 默认值 |
| ----------------------- | -------------------------------------------------------- | ------ |
| config.pageStyle        | 定义整个页面样式属性                                     | {}     |
| config.headerStyle      | 定义页眉样式属性                                         | {}     |
| config.bodyStyle        | 定义主体内容样式属性                                     | {}     |
| config.footerStyle      | 定义页脚样式属性                                         | {}     |
| config.isDetail         | 定义是否为详情模式(即只读模式)，为true时，页面属性可编辑 | false  |
| config.HeaderTitle      | 定义页头标题                                             | ""     |
| config.ContentTitle     | 定义主体内容标题                                         | ""     |
| config.FooterTitle      | 定义页脚标题                                             | ""     |
| config.content          | 定义主体内容列表                                         | []     |
| config.content[i].title | 定义主体内容列表标题                                     | ""     |
| config.content[i].value | 定义主体内容列表值                                       | ""     |
| @getData                | 获取更改数据（输入即更改）                               |        |

##### 实例

```vue
<template>
  <div id="app">
		<cover :config="config" @getData="getData"></cover>
  </div>
</template>

<script>
import cover from './components/cover.vue'
export default {
  name: 'app',
  components: {
		cover
  },
	data(){
		return {
			config:{
				isDetail:false,
				pageStyle:{},
				headerStyle:{},
				bodyStyle:{},
				footerStyle:{},
				HeaderTitle:"封面",
				ContentTitle:"卷宗标题",
				FooterTitle:"卷宗页脚",
				content:[
					{title:"卷宗号",value:"10334441"},
					{title:"创建人",value:"张三"},
					{title:"日期",value:"2021-01-20"},
					{title:"对象",value:"执法人"}
				]
			}
		}
	},
	methods:{
		getData(e){
			console.log(e)
		}
	}
}
</script>

<style>

</style>
```

#### treeMenu

作用：定义一个类似word文档的目录页

##### 调用方法
> 将组件迁移至项目，在页面引入treeMenu所在地址，并在components中直接引入 即可
> 该组件依赖page布局

##### 属性

###### config
子属性

| 属性名                      | 作用                 | 默认值              |
| --------------------------- | -------------------- | ------------------- |
| config.pageStyle            | 定义整个页面样式属性 | {}                  |
| config.headerStyle          | 定义页眉样式属性     | {}                  |
| config.bodyStyle            | 定义主体内容样式属性 | {}                  |
| config.footerStyle          | 定义页脚样式属性     | {}                  |
| config.HeaderTitle          | 定义页头标题         | ""                  |
| config.ContentTitle         | 定义主体内容标题     | ""                  |
| config.FooterTitle          | 定义页脚标题         | ""                  |
| config.list                 | 定义目录列表         | []                  |
| config.list.name            | 定义目录标题         | ""                  |
| config.list.number          | 定义目录所在页码     | ""                  |
| config.list.children        | 定义目录子项         | []                  |
| config.list.children.name   | 定义目录子项标题     | ""                  |
| config.list.children.number | 定义目录子项所在页码 | ""                  |
| jump                        | 点击目录子项的事件   | console.log("跳转") |

##### 实例

```vue
<template>
  <div id="app">
		<tree-menu :config="treeConfig" :jump="jump"></tree-menu>
  </div>
</template>

<script>
import treeMenu from './components/treeMenu.vue'
export default {
  name: 'app',
  components: {
		treeMenu
  },
	data(){
		return {
			treeConfig:{
				pageStyle:{},
				headerStyle:{},
				bodyStyle:{},
				footerStyle:{},
				HeaderTitle:"卷宗页眉",
				ContentTitle:"目录",
				FooterTitle:"卷宗页脚",
				list:[
					{name:"封面",number:"1"},
					{name:"目录",number:"2"},
					{name:"执法单",number:"3",children:[
						{name:"执法单1",number:"4"},
						{name:"执法单2",number:"5"}
					]},
					{name:"整改单",number:"6",children:[
						{name:"整改单1",number:"7"},
						{name:"整改单2",number:"8"}
					]},
					{name:"处罚单",number:"9",children:[
						{name:"处罚单1",number:"10"}
					]},
				]
			}
		}
	},
	methods:{
		jump(){
			console.log("外部跳转事件")
		}
	}
}
</script>

<style>

</style>

```

### page

作用：page布局，定义一个page页,cover和treeMenu的依赖项

##### 调用方法
> 将组件迁移至项目，在页面引入page所在地址，并在components中直接引入 即可

##### 对应的插槽

| 插槽名      | 对应的位置 |
| ----------- | ---------- |
| pageHeader  | 页眉       |
| pageContent | 主体内容   |
| pageFooter  | 页脚       |

##### 属性

| 属性名      | 作用         | 默认值 |
| ----------- | ------------ | ------ |
| pageStyle   | 定义页面     | {}     |
| headerStyle | 页眉样式     | {}     |
| bodyStyle   | 主体内容样式 | {}     |
| footerStyle | 页脚样式     | {}     |

##### 实例

```vue
<!-- 封面组件 -->
<template>
	<page 
	:pageStyle="pageStyle" 
	:headerStyle="headerStyle" 
	:bodyStyle="bodyStyle" 
	:footerStyle="footerStyle">
		<div slot="pageHeader">
            页眉
    </div>
		<div slot="pageContent">
			主题内容
		</div>
		<div slot="pageFooter">
			页脚
		</div>
	</page>
</template>

<script>
	import page from './page.vue'
	export default {
		components:{
			page
		},
		data(){
			return {
                //style属性通用
                pageStyle:{},
                headerStyle:{},
                bodyStyle:{},
                footerStyle:{}
			}
		},
		methods:{
		}
	}
</script>

<style>

</style>

```

